Ruby Cucumber Sample
====================

This project is an example of testing framework for running E2E UI tests. It uses Cucumber and Capybara for describing steps and actions, Allure2 for reporting and GitLab CI for running tests

Setup
-----
```
bundle install 
```

Run Tests
---------
```
rake 
```
GitLab CI 
---------
The project is configured to use GitLab CI, click on badge in project description to view pipeline runs. 